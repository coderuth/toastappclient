package com.codesparts.toastapp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.codesparts.toastapp.adapters.IngredientsAdapter;
import com.codesparts.toastapp.auth.LogInActivity;
import com.codesparts.toastapp.misc.AlertDialogHelper;
import com.codesparts.toastapp.misc.RecyclerItemClickListener;
import com.codesparts.toastapp.misc.TRecyclerView;
import com.codesparts.toastapp.model.Ingredient;
import android.support.design.widget.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements  AlertDialogHelper.AlertDialogListener, NavigationView.OnNavigationItemSelectedListener{

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mDatabase;
    private IngredientsAdapter mAdapter;
    private TRecyclerView recyclerView;
    private TextView itemCount;
    private String mUserId;

    private ArrayList<Ingredient> ingredientList = new ArrayList<>();
    private ArrayList<Ingredient> ingredientSelectedList = new ArrayList<>();
    private FloatingActionButton fab, fabMulti;
    boolean isMultiSelect = false;
    private ActionMode mActionMode;
    private Animation fab_open, fab_close;
    private AlertDialogHelper alertDialogHelper;
    private Vibrator vibe;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private Menu contextMenu;
    private Context context;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        context = this;

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_in);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_out);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

        //TextView emailNav = (TextView) findViewById(R.id.email);

        vibe = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE) ;

        if (mFirebaseUser == null) {
            loadLogInView();
        } else {
            mUserId = mFirebaseUser.getUid();
            recyclerView = (TRecyclerView) findViewById(R.id.recycler_view);

            mAdapter =new IngredientsAdapter(this, ingredientList, ingredientSelectedList);
            recyclerView.setAdapter(mAdapter);

            View emptyView = findViewById(R.id.todo_list_empty_view);
            mAdapter =new IngredientsAdapter(this, ingredientList, ingredientSelectedList);
            recyclerView.setEmptyView(emptyView);
            recyclerView.setAdapter(mAdapter);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(recyclerView.getContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(new DefaultItemAnimator());

            alertDialogHelper = new AlertDialogHelper(this);
            fabMulti = (FloatingActionButton) findViewById(R.id.fabMulti);
            fab      = (FloatingActionButton) findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.custom_dialog);
                    Button btnDone = (Button) dialog.findViewById(R.id.custom_dialog_btn_done);
                    Button btnCancel = (Button) dialog.findViewById(R.id.cancel);

                    final EditText ingText = (EditText) dialog.findViewById(R.id.editText);
                    final EditText qtyText = (EditText) dialog.findViewById(R.id.editText2);

                    btnDone.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Ingredient item = new Ingredient(ingText.getText().toString(), "Category", qtyText.getText().toString());
                            mDatabase.child("users").child(mUserId).child("items").push().setValue(item);
                            Toast.makeText(MainActivity.this, "Added "+ingText.getText(), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    });

                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            });

            fabMulti.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isMultiSelect = false;
                    Intent intent = new Intent(MainActivity.this, SearchResultActivity.class);
                    intent.putParcelableArrayListExtra("ingredient_list", ingredientSelectedList);
                    startActivity(intent);
                }
            });

            mDatabase.child("users").child(mUserId).child("items").addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    ingredientList.add(new Ingredient((String) dataSnapshot.child("title").getValue(), "Category", dataSnapshot.child("quantity").getValue()+" nos."));
                    mAdapter.notifyDataSetChanged();

                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    ingredientList.add(new Ingredient((String) dataSnapshot.child("title").getValue(), "Category", dataSnapshot.child("quantity").getValue()+" nos."));
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    ingredientList.remove(new Ingredient((String) dataSnapshot.child("title").getValue(), "Category", dataSnapshot.child("quantity").getValue()+" nos."));
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    if (isMultiSelect)
                        multiSelect(position);
                    else
                        Toast.makeText(getApplicationContext(), ingredientList.get(position).getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onItemLongClick(View view, int position) {
                    if (!isMultiSelect) {
                        ingredientSelectedList = new ArrayList<>();
                        isMultiSelect = true;
                        if (mActionMode == null) {
                            vibe.vibrate(50);
                            mActionMode = startSupportActionMode(mActionModeCallback);
                        }
                    }
                    multiSelect(position);
                }
            }));
            mAdapter.notifyDataSetChanged();
        }
    }

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_multi_select, menu);
            contextMenu = menu;
            fab.startAnimation(fab_close);
            fab.setVisibility(View.INVISIBLE);
            fabMulti.startAnimation(fab_open);
            getWindow().setStatusBarColor(getResources().getColor(R.color.actionModeColorDark));
            fabMulti.setVisibility(View.VISIBLE);
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
            isMultiSelect = false;
            ingredientSelectedList = new ArrayList<>();
            fab.startAnimation(fab_open);
            fab.setVisibility(View.VISIBLE);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            fabMulti.setVisibility(View.INVISIBLE);
            refreshAdapter();
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_delete:
                    alertDialogHelper.showAlertDialog("Delete Item", "Are you sure you want to Delete the Selected Items", "DELETE", "CANCEL", 1, false);
                    return true;
                case R.id.select_all:
                    if (mActionMode != null) {
                        for(Ingredient singleIngredient : ingredientList) {
                            if (ingredientSelectedList.contains(singleIngredient))
                                continue;
                            else
                                ingredientSelectedList.add(singleIngredient);
                        }

                        if (ingredientSelectedList.size() > 0)
                            mActionMode.setTitle(ingredientSelectedList.size()+"");
                        else
                            mActionMode.setTitle("");
                        refreshAdapter();
                    }
                    return true;
                default:
                    return false;
            }
        }
    };

    public void multiSelect(int position) {
        if (mActionMode != null) {
            if (ingredientSelectedList.contains(ingredientList.get(position)))
                ingredientSelectedList.remove(ingredientList.get(position));
            else
                ingredientSelectedList.add(ingredientList.get(position));

            if (ingredientSelectedList.size() > 0)
                mActionMode.setTitle(ingredientSelectedList.size()+"");
            else
                mActionMode.setTitle("");
            refreshAdapter();
        }
    }

    public void refreshAdapter() {
        mAdapter.selectedIngredientList = ingredientSelectedList;
        mAdapter.ingredientList = ingredientList;
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.logoff) {
            mFirebaseAuth.signOut();
            loadLogInView();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onPositiveClick(int from) {
        if(from == 1) {
            if(ingredientSelectedList.size() > 0) {
                for(Ingredient i : ingredientSelectedList) {
                    ingredientList.remove(i);
                    deleteItem(i);
                }
                mAdapter.notifyDataSetChanged();
                if (mActionMode != null) {
                    mActionMode.finish();
                }
                Toast.makeText(getApplicationContext(), "Delete Click", Toast.LENGTH_SHORT).show();
            }
        }
        else if(from == 2) {
            if (mActionMode != null) {
                mActionMode.finish();
            }
            Ingredient mSample = new Ingredient("Ingredient" + ingredientList.size(), "Category" + ingredientList.size(), "" + ingredientList.size());
            ingredientList.add(mSample);
            mAdapter.notifyDataSetChanged();
        }
    }

    void deleteItem(Ingredient position){
        mDatabase.child("users").child(mUserId).child("items")
                .orderByChild("title")
                .equalTo(position.getTitle())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChildren()) {
                            DataSnapshot firstChild = dataSnapshot.getChildren().iterator().next();
                            firstChild.getRef().removeValue();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) { }
                });
    }

    @Override
    public void onNegativeClick(int from) {}

    @Override
    public void onNeutralClick(int from) {}

    private void loadLogInView() {
        Intent intent = new Intent(this, LogInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }
}
