package com.codesparts.toastapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import com.codesparts.toastapp.adapters.RestaurantAdapter;
import com.codesparts.toastapp.api.APIClient;
import com.codesparts.toastapp.api.APIInterface;
import com.codesparts.toastapp.misc.RecyclerItemClickListener;
import com.codesparts.toastapp.model.Ingredient;
import com.codesparts.toastapp.model.Recipe;
import com.codesparts.toastapp.model.RecipeDetail;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchResultActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private String items="";
    private List<RecipeDetail> recipeList;
    private RecyclerView recyclerView;
    private final String EXTRA_IMAGE = "extraImage";
    private final String EXTRA_TITLE = "extraTitle";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        Bundle bundle = getIntent().getExtras();
        ArrayList<Ingredient> ingredientList = bundle.getParcelableArrayList("ingredient_list");
        for(Ingredient singleIngredient : ingredientList) {
            items = items + singleIngredient.getTitle() + " ";
        }

        initToolbar();
        initFAB();
        initRecyclerView();
        fetchRemoteResource();
        getSupportActionBar().setSubtitle(items);
    }

    private void initRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.movies_recycler_view);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        layoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
//                RecipeDetailActivity.navigate(this, view.findViewById(R.id.image), viewModel);



                Intent intent = new Intent(SearchResultActivity.this, RecipeDetailActivity.class);
                intent.putExtra(EXTRA_IMAGE, recipeList.get(position).getCdnImageUrl());
                intent.putExtra(EXTRA_TITLE, recipeList.get(position).getTitle());
                //intent.putParcelableArrayListExtra(EXTRA_LIST, viewModel.getPreparationSteps());

                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(SearchResultActivity.this, view.findViewById(R.id.image), EXTRA_IMAGE);
                ActivityCompat.startActivity(SearchResultActivity.this, intent, options.toBundle());

//                Intent recipeIntent =new Intent(getApplicationContext(), RecipeDetailActivity.class);
//                recipeIntent.putExtra("recipe", recipeList.get(position));
//                startActivity(recipeIntent);
            }

            @Override
            public void onItemLongClick(View view, int position) {
            }
        }));
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
    }

    private void initFAB() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
    }

    private void fetchRemoteResource() {
        final String uid = "";
        final String lang = "en";
        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        Call<Recipe> call = apiService.getRecipe(items, uid, lang);
        call.enqueue(new Callback<Recipe>() {
            @Override
            public void onResponse(Call<Recipe> call, Response<Recipe> response) {
                int statusCode = response.code();
                recipeList = response.body().getRecipes();
                recyclerView.setAdapter(new RestaurantAdapter(recipeList, R.layout.recipe_result_card, getApplicationContext()));
                recyclerView.scheduleLayoutAnimation();
            }

            @Override
            public void onFailure(Call<Recipe> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_result, menu);
        return true;
    }

}
