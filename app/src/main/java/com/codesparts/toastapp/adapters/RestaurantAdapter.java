package com.codesparts.toastapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codesparts.toastapp.R;
import com.codesparts.toastapp.model.RecipeDetail;
import com.squareup.picasso.Picasso;

import java.util.List;


public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.RecipeViewHolder> {

    private List<RecipeDetail> recipe;
    private int rowLayout;
    private Context context;


    public static class RecipeViewHolder extends RecyclerView.ViewHolder {
        LinearLayout recipeLayout;
        TextView recipeTitle;
        ImageView imgURL;
        TextView time;

        public RecipeViewHolder(View v) {
            super(v);
            recipeLayout = (LinearLayout) v.findViewById(R.id.restaurant_layout);
            recipeTitle = (TextView) v.findViewById(R.id.title);
            imgURL = (ImageView) v.findViewById(R.id.image);
            time = (TextView) v.findViewById(R.id.time);
        }
    }

    public RestaurantAdapter(List<RecipeDetail> recipe, int rowLayout, Context context) {
        this.recipe = recipe;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public RecipeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new RecipeViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RecipeViewHolder holder, final int position) {
        holder.recipeTitle.setText(recipe.get(position).getTitle());
        holder.time.setText(recipe.get(position).getCookingTime());
        Picasso.with(context).load(recipe.get(position).getCdnImageUrl()).into(holder.imgURL);
        //holder.imgURL.setImageResource(recipe.get(position).getCdnImageUrl());
    }

    @Override
    public int getItemCount() {
        return recipe.size();
    }
}
