package com.codesparts.toastapp.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    public static final String BASE_URL = "http://api.dakaar.recipes/v1/";
    private static Retrofit retrofit = null;

//    api.dakaar.recipes/v1/search?search_query=Tomato%20Onion&user_id=&language_name=en
    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}