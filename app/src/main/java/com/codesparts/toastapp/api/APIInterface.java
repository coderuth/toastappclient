package com.codesparts.toastapp.api;

import com.codesparts.toastapp.model.Recipe;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIInterface {
    @POST("search")
    Call<Recipe> getRecipe(
            @Query("search_query") String query,
            @Query("user_id") String uid,
            @Query("language_name") String lang
    );
}

//    @Headers("user-key: 9671c458ff47cf76f76c63285ed8dd94")
//    @GET("geocode")
//    Call<RestaurantResponse> getNearbyRestaurants(
//            @Query("lat") Double latitude,
//            @Query("lon") Double longitude
//    );
//    api.dakaar.recipes/v1/search?search_query=Tomato%20Onion&user_id=&language_name=en

//    @GET("movie/{id}")
//    Call<AMoviesResponse> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey);
