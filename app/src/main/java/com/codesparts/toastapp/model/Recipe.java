
package com.codesparts.toastapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Recipe {

    @SerializedName("recipes")
    @Expose
    private List<RecipeDetail> recipes = null;

    public List<RecipeDetail> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<RecipeDetail> recipes) {
        this.recipes = recipes;
    }

}
