package com.codesparts.toastapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RecipeDetail implements Parcelable {

    @SerializedName("cdn_image_url")
    @Expose
    private String cdnImageUrl;
    @SerializedName("cooking_time")
    @Expose
    private String cookingTime;
    @SerializedName("ingredients")
    @Expose
    private List<String> ingredients = null;
    @SerializedName("number_of_serving")
    @Expose
    private String numberOfServing;
    @SerializedName("preparation_steps")
    @Expose
    private List<String> preparationSteps = null;
    @SerializedName("prerequisite_preparation_time")
    @Expose
    private String prerequisitePreparationTime;
    @SerializedName("recipe_favourite_count")
    @Expose
    private String recipeFavouriteCount;
    @SerializedName("recipe_id")
    @Expose
    private String recipeId;
    @SerializedName("tags")
    @Expose
    private List<String> tags = null;
    @SerializedName("title")
    @Expose
    private String title;

    public String getCdnImageUrl() {
        return cdnImageUrl;
    }

    public void setCdnImageUrl(String cdnImageUrl) {
        this.cdnImageUrl = cdnImageUrl;
    }

    public String getCookingTime() {
        return cookingTime;
    }

    public void setCookingTime(String cookingTime) {
        this.cookingTime = cookingTime;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public String getNumberOfServing() {
        return numberOfServing;
    }

    public void setNumberOfServing(String numberOfServing) {
        this.numberOfServing = numberOfServing;
    }

    public List<String> getPreparationSteps() {
        return preparationSteps;
    }

    public void setPreparationSteps(List<String> preparationSteps) {
        this.preparationSteps = preparationSteps;
    }

    public String getPrerequisitePreparationTime() {
        return prerequisitePreparationTime;
    }

    public void setPrerequisitePreparationTime(String prerequisitePreparationTime) {
        this.prerequisitePreparationTime = prerequisitePreparationTime;
    }

    public String getRecipeFavouriteCount() {
        return recipeFavouriteCount;
    }

    public void setRecipeFavouriteCount(String recipeFavouriteCount) {
        this.recipeFavouriteCount = recipeFavouriteCount;
    }

    public String getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    private RecipeDetail(Parcel in) {
        cdnImageUrl = in.readString();
        cookingTime = in.readString();
        if (in.readByte() == 0x01) {
            ingredients = new ArrayList<String>();
            in.readList(ingredients, String.class.getClassLoader());
        } else {
            ingredients = null;
        }
        numberOfServing = in.readString();
        if (in.readByte() == 0x01) {
            preparationSteps = new ArrayList<String>();
            in.readList(preparationSteps, String.class.getClassLoader());
        } else {
            preparationSteps = null;
        }
        prerequisitePreparationTime = in.readString();
        recipeFavouriteCount = in.readString();
        recipeId = in.readString();
        if (in.readByte() == 0x01) {
            tags = new ArrayList<String>();
            in.readList(tags, String.class.getClassLoader());
        } else {
            tags = null;
        }
        title = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cdnImageUrl);
        dest.writeString(cookingTime);
        if (ingredients == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(ingredients);
        }
        dest.writeString(numberOfServing);
        if (preparationSteps == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(preparationSteps);
        }
        dest.writeString(prerequisitePreparationTime);
        dest.writeString(recipeFavouriteCount);
        dest.writeString(recipeId);
        if (tags == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(tags);
        }
        dest.writeString(title);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<RecipeDetail> CREATOR = new Parcelable.Creator<RecipeDetail>() {
        @Override
        public RecipeDetail createFromParcel(Parcel in) {
            return new RecipeDetail(in);
        }

        @Override
        public RecipeDetail[] newArray(int size) {
            return new RecipeDetail[size];
        }
    };
}